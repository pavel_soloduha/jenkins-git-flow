import java.util.regex.*
// import hudson.scm.ChangeLogSet.Entry
// import hudson.scm.*
import org.jenkinsci.plugins.workflow.support.steps.build.*

pipeline {

  agent {
    dockerfile {
      filename 'Dockerfile'
      dir 'jenkins-agent-image'
      args '-u root -v /var/run/docker.sock:/var/run/docker.sock'
    }
  }

	stages {

		stage("Check do we need to run") {
			steps {
				script {
					def mapCommitToMsg = getMapCommitToMsg(currentBuild)
					mapCommitToMsg.each { key, value ->
			    	print key + ": " + value
					}
					if (mapCommitToMsg.size() == 1) {
				    print "size=1"
						mapCommitToMsg.each { key, value ->
	        		if (value ==~ /(Move project to version.*)/) {
	        			print "Abort build"
	        			currentBuild.result = "Aborted"
	        			exit
	        		}
						}
					}
				}
			}
		}


		stage("Test project") {
			steps {
				sh "mvn clean test -q"
			}
		}

		stage("Build project") {
			steps {
				sh "mvn clean install -DskipTests=true -q"
			}
		}

		stage("Choose docker image tag") {
			steps {
				script {
					if (BRANCH_NAME ==~ /(PR-.*)/) {
						print "it's PR"
						print CHANGE_BRANCH
						print BRANCH_NAME
						// for docker_tag use issue_number + PR + build_number
						// examples:
						// for existing PR from branch_name: feature/CM-1235-bla-bla-bla and build_number: 5 => docker_tag: CM-1235-PR-b-5
						DOCKER_TAG = (CHANGE_BRANCH =~ /(CM\-\d+)/)[0][1] + "-PR-b-" + "${BUILD_NUMBER}"

          } else if (BRANCH_NAME ==~ /(feature\/.*)/ || BRANCH_NAME ==~ /(bugfix\/.*)/ || BRANCH_NAME ==~ /(hotfix\/.*)/) {
          	print "it's feature/bugfix/hotfix branch"
						print BRANCH_NAME
						// for docker_tag use issue_number + build_number
						// examples:
						// for branch_name: feature/CM-1235-bla-bla-bla and build_number: 5 => docker_tag: CM-1235-b-5
						DOCKER_TAG = (BRANCH_NAME =~ /(CM\-\d+)/)[0][1] + "-b-" + "${BUILD_NUMBER}"

          } else if (BRANCH_NAME ==~ /(v\d+\.\d+\.\d+-RC-\d+)/) {
          	print "it's tag on release branch"
          	print BRANCH_NAME
						DOCKER_TAG = (BRANCH_NAME =~ /(v\d+\.\d+\.\d+-RC-\d+)/)[0][1] + "-b-" + "${BUILD_NUMBER}"

          } else if (BRANCH_NAME ==~ /(v\d+\.\d+\.\d+)/) {
          	print "it's tag on master branch"
          	print BRANCH_NAME
						DOCKER_TAG = (BRANCH_NAME =~ /(v\d+\.\d+\.\d+)/)[0][1] + "-b-" + "${BUILD_NUMBER}"

        	} else if (BRANCH_NAME == "develop"){
        		//if branch_name: develop => docker_tag: latest
            DOCKER_TAG = "latest"
            
          } else if (BRANCH_NAME == "master" || BRANCH_NAME == "release") {
          	// for release and master => docker_tag: default
          	DOCKER_TAG = "default"

          	def mapCommitToBranch = getMapCommitToBranch(currentBuild)
						
						withCredentials([usernamePassword(credentialsId: 'bitbucket_solodukha', passwordVariable: 'password', usernameVariable: 'username')]) {
							sh "git config user.email \"${username}\""
							sh "git config user.name \"pavel_soloduha\""

 							def exit_code = sh returnStatus: true, script: "git checkout develop"
 							if (exit_code == 1) {
								sh "git fetch origin develop:refs/remotes/origin/develop"
								sh "git pull"
 							}
							sh "git checkout -B develop"
							sh "git reset --hard origin/develop"
							sh returnStatus: true, script: "git branch --track origin/develop"

							//create feature branch to backport changes from hotfix/ and bugfix/ branches
							mapCommitToBranch.each { key, value ->
		        		sh "git checkout develop"
								sh "git reset --hard origin/develop"
								def branch_name = value.split(" ")[0]
								sh "git checkout -B feature/${branch_name}"
								exit_code = sh returnStatus: true, script: "git cherry-pick -x --allow-empty --allow-empty-message ${key}"
								if (exit_code == 1) {
									sh "git add -A"
								}
								sh "git commit --allow-empty -m \"${value}\""
								sh "git push https://pavel_soloduha:${password}@bitbucket.org/pavel_soloduha/jenkins-git-flow.git"
							}

							// increase project version and add git tag to build it
							sh returnStatus: true, script: "git checkout -B ${BRANCH_NAME}"
							sh "git reset --hard origin/${BRANCH_NAME}"

							PROJECT_VERSION_OLD = sh returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -DforceStdout -q"
							print PROJECT_VERSION_OLD
							PROJECT_VERSION_NEW = ""

							if (PROJECT_VERSION_OLD ==~ /(.*RC.*)/) {
								print "release branch"
								// release branch
								// M.m.p-RC-N => M.m.p-RC-(N+1)
								PROJECT_VERSION_NEW = (PROJECT_VERSION_OLD =~ /(\d+\.\d+\.\d+-RC-\d+)/)[0][1]
								print PROJECT_VERSION_NEW
								def splitted_version = PROJECT_VERSION_NEW.split("-")
								def postfix_number = splitted_version[2] as Integer
								postfix_number = postfix_number + 1
								PROJECT_VERSION_NEW = splitted_version[0] + "-RC-" + postfix_number
								print PROJECT_VERSION_NEW
							} else {
								print "master branch"
								// master branch
								// M.m.p = > M.m.(p+1)
								def major_number = (PROJECT_VERSION_OLD =~ /(\d+)/)[0][1] as Integer
								def minor_number = (PROJECT_VERSION_OLD =~ /(\d+)/)[1][1] as Integer
								def patch_number = (PROJECT_VERSION_OLD =~ /(\d+)/)[2][1] as Integer
								patch_number = patch_number + 1
								PROJECT_VERSION_NEW = major_number + "." + minor_number + "." + patch_number
								print PROJECT_VERSION_NEW
							}
						
							sh "git config user.email \"${username}\""
							sh "git config user.name \"pavel_soloduha\""
							sh "mvn versions:set -DnewVersion=${PROJECT_VERSION_NEW} -DgenerateBackupPoms=false -q"
							sh "git add -A"
							sh "git commit -m \"Move project to version ${PROJECT_VERSION_NEW}\""
							sh "git tag v${PROJECT_VERSION_NEW}"
							sh "git push --set-upstream https://pavel_soloduha:${password}@bitbucket.org/pavel_soloduha/jenkins-git-flow.git ${BRANCH_NAME}"
							sh "git push https://pavel_soloduha:${password}@bitbucket.org/pavel_soloduha/jenkins-git-flow.git --tags"
						}
          } else {
          	DOCKER_TAG = "default"
          }
          print DOCKER_TAG
				}
			}
		}

		stage("Build docker image") {
			when {
				expression {
					DOCKER_TAG != "default"
				}
			}
			steps {
				script {
					sh "docker build . --rm -t pavelsoloduha/flow-project:${DOCKER_TAG}"
				}
			}
		}

		stage("Publish docker image") {
			when {
				expression {
					DOCKER_TAG != "default"
				}
			}
			steps {
				script {
					withCredentials([usernamePassword(credentialsId: 'dockerhub_solodukha', passwordVariable: 'password', usernameVariable: 'username')]) {
				    sh "docker login -u ${username} -p ${password}"
				    sh "docker push pavelsoloduha/flow-project:${DOCKER_TAG}"
				    if (BRANCH_NAME == "develop") {
				    	PROJECT_VERSION = sh returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -DforceStdout -q"
				    	sh "docker tag pavelsoloduha/flow-project:${DOCKER_TAG} pavelsoloduha/flow-project:${PROJECT_VERSION}-b-${BUILD_NUMBER}"
				    	sh "docker push pavelsoloduha/flow-project:${PROJECT_VERSION}-b-${BUILD_NUMBER}"
				    	sh "docker rmi pavelsoloduha/flow-project:${PROJECT_VERSION}-b-${BUILD_NUMBER}"
				    }
				    sh "docker rmi pavelsoloduha/flow-project:${DOCKER_TAG}"
						sh "docker logout"
					}
				}
			}
		}
	}
	
	post {
		always {
			cleanWs notFailBuild: true
			sh returnStatus: true, script: "rm -rf ./*"
			sh returnStatus: true, script: "rm -rf ./.*"
		}
	}
}

@NonCPS
def getMapCommitToMsg(RunWrapper runWrapper) {
	def mapCommitToMsg = [:]
	runWrapper.getChangeSets().each { changes ->
  	changes.each { change -> 
  		def message = change.getMsg()
			def commitId = change.getCommitId()
			mapCommitToMsg[commitId] = message
  	}
  }
  return mapCommitToMsg
}

@NonCPS
def getMapCommitToBranch(RunWrapper runWrapper) {
	def mapCommitToBranch = [:]
	runWrapper.getChangeSets().each { changes ->
  	changes.each { change -> 
  		def message = change.getMsg()
  		if (message ==~ /(.*hotfix\/.*)/ || message ==~ /(.*bugfix\/.*)/) {
  			print "need to cherrypick"
  			def commitId = change.getCommitId()
  			Pattern p = Pattern.compile("((?<=.*hotfix/).*)|((?<=.*bugfix/).*)");
  			// print message
				Matcher m = p.matcher(message);
				m.find()
				def branch_name = m.group()
				print commitId
				print branch_name
				mapCommitToBranch[commitId] = branch_name

				change.properties.each { prop ->
					println prop
				}
  		}
  	}
  }
  return mapCommitToBranch
}